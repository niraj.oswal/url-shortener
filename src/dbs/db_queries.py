from src.utils.exception_handler import ResourceNotFound


class DbHandler:

    def __init__(self, connection):
        self.connection = connection

    async def check_presence_of_url(self, original: str) -> bool:
        query = 'select * from record where original_url = $1'
        result = await self.connection.fetch(query, original)
        return True if len(result) > 0 else False

    async def insert(self, original_url, shortened_url: str) -> None:
        connection = self.connection
        present = await self.check_presence_of_url(original_url)
        if not present:
            print("not present")
            query = 'insert into record(original_url,shortened_url) values($1,$2)'
            await connection.execute(query, original_url, shortened_url)
        else:
            print("present")

    async def return_original_url(self, shortened_url: str) -> str:
        query = 'select * from record where shortened_url = $1'

        result = await self.connection.fetch(query, shortened_url)
        print(list(result))
        if len(result) == 0:
            raise ResourceNotFound

        result_list = list(map(dict, result))
        original_url = result_list[0]['original_url']
        return original_url
