import asyncpg
from src import config

DB_CONFIG = config.DB_CONFIG


class Database:
    __instance = None

    @staticmethod
    async def get_instance():
        """ Static access method. """
        if Database.__instance is None:
            database = Database()
            await database.establish_connection()
            await database.create_table()

        return Database.__instance

    def __init__(self):
        """ Virtually private constructor. """
        if Database.__instance is not None:
            raise Exception("This class is a singleton!")
        else:
            self.connection = None
            Database.__instance = self

    async def establish_connection(self) -> None:
        # print(self.name)
        print("Establishing connection with DB ({}) ".format(
            DB_CONFIG.database,
        )
        )

        try:
            self.connection = await asyncpg.connect(
                user=DB_CONFIG.user,
                password=DB_CONFIG.password,
                database=DB_CONFIG.database,
                host='0.0.0.0'
            )
            print(self.connection)
            print("Connection to postgresql db established!")
        except Exception as e:
            print(e)

    async def create_database(self) -> None:
        conn = await asyncpg.connect(
            user='postgres',
            database='postgres',
            password='12345678'
        )
        print(conn)
        await conn.execute(
            f'CREATE DATABASE "{DB_CONFIG.database}" OWNER "{DB_CONFIG.user}"'
        )
        await conn.close()

    async def create_table(self) -> None:
        conn = self.connection

        await conn.execute(
            '''CREATE TABLE IF NOT EXISTS record (
                 original_url text unique,
                 shortened_url text unique
            )'''
        )

#
# async def database_connection():
#     # print(app.name)
#     conn = await asyncpg.connect(user=DB_CONFIG.user, password=DB_CONFIG.password,
#                                  database=DB_CONFIG.database, host='0.0.0.0')
#     print("connected11")
#     return conn
