
import unittest
from src.utils import url_shortener
from src.test.utils.async_test_utils import my_async_test


class TestFunction(unittest.TestCase):
    @my_async_test
    async def test_url_shortener_utils(self):
        result = await url_shortener.generate_hash("sdff://skfdjkjekvvjvjv.sdvds.vsf")
        self.assertEqual(len(result), 6)
        self.assertTrue(isinstance(result, str))


if __name__ == '__main__':
    print("niraj")
    unittest.main()
