import asyncio


def my_async_test(func):
    def test_wrapper(*args, **kwargs):
        loop = asyncio.new_event_loop()
        loop.run_until_complete(func(*args, **kwargs))
        loop.close()

    # change the function name to avoid clashes (unittest requires test funcs to have different names)
    test_wrapper.__name__ += func.__name__
    return test_wrapper
