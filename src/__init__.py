from sanic import Sanic

from src.dbs import Database
from src.routes import blueprint


app = Sanic("My first app")
app.blueprint(blueprint)


@app.listener("before_server_start")
async def setup(app, loop) -> None:
    database = await Database.get_instance()
    print("connected before server start")
