from sanic import response
from functools import wraps
from jsonschema import ValidationError


class InvalidUrlException(Exception):

    def __init__(self, message="URL is not valid"):
        self.message = message
        super().__init__(self.message)

    def __str__(self):
        return f'{self.message}'


class ResourceNotFound(Exception):
    def __init__(self, message="URL not found"):
        self.message = message
        super().__init__()

    def __str__(self):
        return f'{self.message}'


def handle_api_exception(func):
    @wraps(func)
    async def wrap(*args, **kwargs):
        try:
            status = 200
            resp = await func(*args, **kwargs)

        except (ValidationError, InvalidUrlException) as e:
            error_resp = {
                "message": e.message
            }
            resp = error_resp
            status = 400

        except ResourceNotFound as e:
            print(e)
            error_resp = {
                "message": e.message
            }
            resp = error_resp
            status = 404

        except Exception as e:
            status = 500
            print(e, "rtrt")
            error_resp = {
                "message": "internal serval error"
            }
            resp = error_resp

        return response.json(status=status, body=resp)

    return wrap
