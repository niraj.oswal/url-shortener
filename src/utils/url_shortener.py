import hashlib


async def generate_hash(long_url: str) -> str:
    encryptor = hashlib.shake_256()
    encryptor.update(bytes(long_url, 'utf-8'))
    result = encryptor.hexdigest(3)
    print(result)
    return result
