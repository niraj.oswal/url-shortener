SHORTENER_VALIDATION = {
    "type": "object",
    "required": ["url"],
    "properties": {
        "url": {
            "type": "string"
        }
    }
}

GET_ORIGINAL_URL_VALIDATION = {
    "type": "object",
    "required": ["shortenedUrl"],
    "properties": {
        "shortenedUrl": {
            "type": "string"
        }
    }
}