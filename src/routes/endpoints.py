from sanic import Blueprint, Request
from src.utils.url_shortener import generate_hash
from src import config
from src.utils.exception_handler import InvalidUrlException, handle_api_exception
from src.dbs import Database
from src.dbs.db_queries import DbHandler
import jsonschema
import validators
from src.utils.schemas import SHORTENER_VALIDATION, GET_ORIGINAL_URL_VALIDATION

my_blueprint = Blueprint("my blueprint")


@my_blueprint.route('/url', methods=['POST'])
@handle_api_exception
async def url_shortener(request: Request) -> dict:
    request_body = request.json
    jsonschema.validate(request_body, SHORTENER_VALIDATION)
    if not validators.url(request_body['url']):
        raise InvalidUrlException("URL is not valid")

    # try:
    #     jsonschema.validate(request_body, SHORTENER_VALIDATION)
    #     if not validators.url(request_body['myurl']):
    #         raise UrlException("URL is not valid")
    # except Exception as e:
    #     return response.json({
    #         "message": [e.message]}, 401)

    url = request_body['url']
    url_hash = await generate_hash(url)
    shortened_url = "{}{}".format(config.URL_PREFIX, url_hash)

    DB = await Database.get_instance()
    handler = DbHandler(DB.connection)
    await handler.insert(url, shortened_url)

    result = {
        "shortened_url": shortened_url
    }
    return result


@my_blueprint.route('/url', methods=['GET'])
@handle_api_exception
async def get_original_url(request: Request) -> dict:
    request_body = request.json
    jsonschema.validate(request_body, GET_ORIGINAL_URL_VALIDATION)
    if not validators.url(request_body['shortenedUrl']):
        raise InvalidUrlException("URL is not valid")

    # try:
    #     jsonschema.validate(request_body, GET_ORIGINAL_URL_VALIDATION)
    #     if not validators.url(request_body['shortenedUrl']):
    #         raise UrlException("URL is not valid")
    #
    # except Exception as e:
    #     return response.json({
    #         "message": [e.message]}, 401)

    shortened_url = request_body['shortenedUrl']

    DB = await Database.get_instance()
    handler = DbHandler(DB.connection)
    original_url = await handler.return_original_url(shortened_url)

    result = {
        "original_url": original_url
    }

    return result
