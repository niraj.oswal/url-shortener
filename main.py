import asyncio

from sanic import Request

from src import app


@app.middleware("request")
async def request_middleware(request: Request) -> None:
    print(" Gone through middleware ")


if __name__ == "__main__":
    print("niraj23")
    # debug logs enabled with debug = True
    app.run(host="0.0.0.0", port=8000, debug=True)
