#URL_SHORTENER

Service contains two endpoints.

1.One to convert your original long url into tiny url  
2.Second to return the original url of your tiny url

###Endpoint 1
URI : /url 
method = 'POST'  
body = {
          "url" : " ",
       }  
response = {
              "shortened_url" : " ",
           }


###Endpoint 2
URI : /url  
method = 'GET'  
body = {
          "shortenedUrl" : " ",
       }  
response = {
              "originalUrl" : " "
           }



